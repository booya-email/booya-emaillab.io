---
title: Where is my data stored?
date: 2022-06-30
publishdate: 2022-06-30
---

The data that you input in the Settings Tab is _exclusively_ stored in the local storage of your browser. It never leaves your computer. The data will persist as long as you do not uninstall your browser.

When you browse the aliases for a domain, Booya! pulls the list of aliases each time. Nothing gets permanently stored on your browser's storage or computer. When you close the popup window of the Web Extension, the list of aliases is deleted.