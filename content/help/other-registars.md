---
title: Will you support other domain name providers?
date: 2022-06-30
publishdate: 2022-06-30
---

Yes! A lot of domain name providers offer email forwarding when you buy a domain name from them. We are looking at the ones that provide a simple API so that they can be easily integrated with Booya!.

Please do share details of domain name providers that you'd like to see supported by opening an issue in [Gitlab]({{< param issue_reporting_url >}}).

