---
title: Why was Booya! created?
date: 2022-06-30
publishdate: 2022-06-30
---

#### Problem Statement

It's becoming harder and harder to protect your privacy online. By exposing your private email address online you are providing a (stable) identifier that ad-reliant businesses can use to profile by connecting information from several websites. And what about the spam, clutering your inbox with all sorts of unsollicited emails that you can't even unsubscribe from. Once your private email is out there, there is very little you can do...

#### A Piece Of The Solution

Booya! will not solve all these problems. But it will help you address one specific issue: not sharing your private email address when you don't absolutely need to. It's certainly not the perfect solution but it's a simple and purposeful one, that you can host yourself.

#### How Does It Work?

Booya! let's you create email aliases on the fly whenever you need them. You can think of them as throwaway email addresses. Imagine you want to book a weekend trip with you familly. Instead of providing your private email, you generate an alias with Booya! and use it instead. When your booking is done (or after your weekend) you can simply delete this alias to stop receiving emails and to avoid profiling. Next time you visit that booking website you can (should!) create a new alias.



