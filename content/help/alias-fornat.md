---
title: Can you explain the format of aliases?
date: 2022-06-30
publishdate: 2022-06-30
---

Currently, email aliases look like this: 

`izfPb6zIri-booking.com@my-private-domain.com`

They are made of 3 _pieces_:

1. A random string (generated automatically): `izfPb6zIri`. 
2. The address of the website that you are generating the alias for: `-booking.com`. 
3. The domain name used to create the alias: `@my-private-domain.com`.

The second piece allows Booya! to group aliases in the Browse tab making it possible to manage them easily without requiring a database.