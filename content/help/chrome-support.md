---
title: Will you support Chrome-based browsers?
date: 2022-06-30
publishdate: 2022-06-30
---

Yes! There's no defined release date but we are working on it. We want to have a single codebase for both platforms.

