---
title: How to fill in the Settings tab?
date: 2022-06-30
publishdate: 2022-06-30
---

The Settings tab is where you configure Booya! Web Extension.

#### 1. Alias Domain Names

This is the list of domain names that you will use to create aliases. It is a comma-separated list of domains, i.e. `my-alias-domain.com, other-domain.net`.

These domains need to be registered with [Gandi.net](https://gandi.net).

#### 2. API Key

At [Gandi.net](https://gandi.net) this is called the _Production API key_. It is used to authenticate the API calls made by Booya! If you do not have one yet you can generate (a new) one in your account Security settings: `https://account.gandi.net/en/users/<GANDI HANDLE>/security`.

Make sure to store this somewhere secure since it is only displayed when it is first generated.

#### 3. Destination Email

This is the email to which emails sent to your aliases will be redirected to. Normally this would be the email that you want to keep private.